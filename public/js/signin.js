function initApp() {

    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function() {

        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(e){
            window.location.href = "lobby.html";
        }).catch(function(error) {
            create_alert("error" , error.message);
        });
        txtEmail.value = "";
        txtPassword.value = "";
    });

    btnGoogle.addEventListener('click', function() {

        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {

            var token = result.credential.accessToken;
            var user = result.user;

            window.location.href = "lobby.html";
          }).catch(function(error) {
            create_alert('error', error.message);
        });

    });

    btnSignUp.addEventListener('click', function() {

        firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(e){
            create_alert('success', e.message);
        }).catch(function(error) {
            create_alert('error', error.message);
        });
        txtEmail.value = "";
        txtPassword.value = "";
    });
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function() {
    initApp();
};
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var logout = document.getElementById('logout-btn');
        logout.addEventListener('click', function(){
            firebase.auth().signOut().then(function() {
                window.location.href = "index.html";
                alert('success');
              }).catch(function(error) {
                alert('error');
            });
        })
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

   

    post_btn.addEventListener('click', function() {
        if (post_txt.value != "") {
            post_txt.value= post_txt.value.replace(/&/g, "&amp");
            post_txt.value= post_txt.value.replace(/</g, "&lt");
            var plRef = firebase.database().ref('com_list');
            var newPostRef = plRef.push({
                email: user_email,
                data: post_txt.value
            });
            post_txt.value = "";
            
        }
    });


    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('com_list');

    var total_post = [];

    var first_count = 0;
 
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {


            snapshot.forEach(function(data){

                var childData = data.val();
                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content
                document.getElementById('post_list').innerHTML = total_post.join('');
                first_count = first_count + 1;
            });


            document.getElementById('post_list').innerHTML = total_post.join('');


            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });



        })
        .catch(e => console.log(e.message));
}

window.onload = function() {
    init();
};
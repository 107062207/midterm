# Software Studio 2020 Spring Midterm Project


## Topic
* Project Name : midterm
* Key functions (add/delete)
    1. sign in sign out
    2. Public chatroom
    
* Other functions (add/delete)
    1. Third-Party Sign In
    2. html code replacing


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%||
|RWD|15%||
|Topic Key Function|20%||

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%||

## Website Detail Description

# 作品網址：https://midterm-18a63.web.app/

# Components Description : 
1. Membership Mechanism	 : 從首頁進入登入介面後，可以登入既有帳號也可以註冊一個，同時提供google登入。順序是首頁(點擊左上按鈕)->登入/註冊->聊天室
2. RWD : 我的首頁會在寬度縮小至大約700px以下後重新排版，而登入介面則是會一邊縮小一邊更改排版。




